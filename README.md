# Metafacture on Kafka

Experimental Docker image providing a Metafacture workflow on Kafka

## Usage

1. Change path to data directory in service `marcxml-kafka-producer`
2. `docker-compose up`
3. Connect with another Kafka instance to the Docker network in order to
   consume the `resource` topic
