"kafka:9092"|
read-kafka(props="/app/config/consumer.properties", topics="cbs")|
decode-xml|
handle-marcxml|
filter(FLUX_DIR + "245aFilter.xml")|
morph(FLUX_DIR + "resourceMorph.xml")|
change-id|
encode-esbulk(escapeChars="true", header="false", index="null", type="bibliographicResource")|
write-kafka(props="/app/config/producer.properties", topics="resource");
