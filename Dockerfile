FROM openjdk:8
WORKDIR /build
RUN git clone https://gitlab.com/swissbib/linked/swissbib-metafacture-commands
WORKDIR /build/swissbib-metafacture-commands
RUN git fetch -a && git checkout renew-kafka-adapter
RUN ./gradlew -q --no-scan :metafacture-kafka:shadowJar
RUN ./gradlew -q --no-scan :metafacture-linkeddata:shadowJar
RUN ./gradlew -q --no-scan :metamorph:shadowJar

FROM sschuepbach/metafacture-runner
ADD config/ /app/config
ADD mfwf/ /mfwf
COPY --from=0 "/build/swissbib-metafacture-commands/metafacture-linkeddata/build/libs/*.jar" "/build/swissbib-metafacture-commands/metafacture-kafka/build/libs/*.jar" "/build/swissbib-metafacture-commands/metamorph/build/libs/*.jar" /app/plugins/
ENTRYPOINT ["/bin/bash", "/app/flux.sh"]
CMD ["/app/config/workflow.flux"]
